package com.organizadorimagenes.geovanny.organizadorimagenes;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by geovanny on 01/06/17.
 */

public class MethodPixel implements Serializable{
    private String hash;
    private ArrayList<String> images;
    private String name;

    public MethodPixel(String hash){
        this.setHash(hash);
        this.setName(hash);
        images = new ArrayList<>();
    }


    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public ArrayList<String> getImages() {
        return images;
    }

    public void setImages(ArrayList<String> images) {
        this.images = images;
    }

    public void agregarImagen(String imagen){
        images.add(imagen);
    }

    public String buscarImagen(String imagen){
        for (int i=0; i<images.size(); i++){
            if (images.get(i) == imagen)
                return images.get(i);
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
