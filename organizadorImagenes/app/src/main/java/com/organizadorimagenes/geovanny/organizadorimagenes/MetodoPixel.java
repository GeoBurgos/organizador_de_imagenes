package com.organizadorimagenes.geovanny.organizadorimagenes;


/**
 * Created by geovanny on 24/05/17.
 */
import android.graphics.Bitmap;

import java.util.Hashtable;
import java.util.ArrayList;

public class MetodoPixel {

    public ArrayList<ArrayList<Integer>> planes = new ArrayList<>();
    public Hashtable<String, ArrayList<Bitmap>> hashBucket = new Hashtable< >();

    public void createPlanes(){
        for(int i=0; i<10; i++){
            ArrayList<Integer> plane = new ArrayList<>();
            for(int j=0; j<65536; j++)
                plane.add((int)(Math.random()*513)-256);
            planes.add(plane);
        }
    }

    public Bitmap resizeImage(Bitmap img){
        return Bitmap.createScaledBitmap(img, 256, 256, true);
    }

    public void viewPlanes(){
        for(int i=0; i<planes.size(); i++)
            System.out.println("Plano"+i+": "+planes.get(i));
    }

    public String hashImagen(ArrayList<Integer> pixImagen){
        String hash = "";
        long productoPunto = 0;
        for(int i=0; i<planes.size(); i++){
            for(int j=0; j<pixImagen.size(); j++){
                productoPunto+= planes.get(i).get(j)*pixImagen.get(j);
            }
            if(productoPunto>=0){
                hash+="1";
            }else{
                hash+="0";
            }
            productoPunto=0;
        }
        return hash;
    }

    public void insertKey_Image(String hashImagen, Bitmap image){
        if (hashBucket.containsKey(hashImagen)) {
            hashBucket.get(hashImagen).add(image);
        }else{
            ArrayList<Bitmap> imagesBucket = new ArrayList<>();
            imagesBucket.add(image);
            hashBucket.put(hashImagen, imagesBucket);
        }
    }
}
