package com.organizadorimagenes.geovanny.organizadorimagenes;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.support.constraint.solver.SolverVariable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //Necesitamos un Boton y un imageView
    private ImageButton btn_hacerfoto;
    private ImageView img;
    //variables para saber si usar camera o galeria
        Integer REQUEST_CAMERA=1, SELECT_FILE=0;
    String FolderPath;
    private Uri file;
    //private MetodoPixel metodoPixel = new MetodoPixel();
    private ListView buckList;
    private ArrayAdapter<String> buckAdapter;
    private ArrayList<String> buckArrayList = new ArrayList<>();

    private ArrayList<MetodoBinario> bucketsBinario = new ArrayList<>();
    private ArrayList<MethodPixel> bucketsPixel = new ArrayList<>();

    public ArrayList<ArrayList<Integer>> planes = new ArrayList<>();
    public ArrayList<ArrayList<Integer>> planesPixel = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Relacionamos con el XML
        createPlanes();
        createPlanesPixel();
        /*buckList = (ListView) this.findViewById(R.id.listBuckets);
        buckAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, buckArrayList);
        buckList.setAdapter(this.buckAdapter);
        for(int i=0; i<15; i++)
            buckArrayList.add(Integer.toString(i));
        buckAdapter.notifyDataSetChanged();
        buckList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                startActivity(intent);
            }
        });*/
        img = (ImageView)this.findViewById(R.id.imageButton);
        btn_hacerfoto = (ImageButton) this.findViewById(R.id.imageButton);
        //Añadimos el Listener Boton
        btn_hacerfoto.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {SelectImage();}});

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    public void SelectImage(){
        final CharSequence[] items={"Camara", "Galeria", "Cancelar"};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Añadir Imagen");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i){
                if (items[i].equals("Camara")){
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File folder = Environment.getExternalStoragePublicDirectory("/AppFolder");
                    //Si no existe se crea
                    if(!folder.exists()) {folder.mkdir();}
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    String imageFileName = timeStamp + ".jpg";
                    File image = new File(folder, imageFileName);
                    file = Uri.fromFile(image);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT,file);
                    startActivityForResult(intent,REQUEST_CAMERA);
                }else if (items[i].equals("Galeria")){
                    Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent,SELECT_FILE);
                }else if (items[i].equals("Cancelar")){
                    dialogInterface.dismiss();
                }
            }
        }).show();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_camera) {

        } else if (id == R.id.methodPixel) {
            addBucketsPixel();
        } else if (id == R.id.methodLBP) {
            agregarBucketsLBP();
        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void addBucketsPixel(){
        buckArrayList.clear();
        buckList = (ListView) this.findViewById(R.id.listBuckets);
        buckAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, buckArrayList);
        buckList.setAdapter(this.buckAdapter);
        for(int i=0; i<bucketsPixel.size(); i++) {
            buckArrayList.add(bucketsPixel.get(i).getName());
            System.out.println("GetName : "+bucketsPixel.get(i).getName());
        }
        buckAdapter.notifyDataSetChanged();
        buckList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                MethodPixel metPixel = bucketsPixel.get(position);
                String method = "MethodPix";
                intent.putExtra("indicador",method);
                intent.putExtra("metPix", metPixel);
                startActivity(intent);
            }
        });
    }

    public void agregarBucketsLBP(){
        buckArrayList.clear();
        buckList = (ListView) this.findViewById(R.id.listBuckets);
        buckAdapter = new ArrayAdapter<>(getApplicationContext(), android.R.layout.simple_spinner_dropdown_item, buckArrayList);
        buckList.setAdapter(this.buckAdapter);
        for(int i=0; i<bucketsBinario.size(); i++)
            buckArrayList.add(bucketsBinario.get(i).getName());
        buckAdapter.notifyDataSetChanged();
        buckList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(getApplicationContext(), Main2Activity.class);
                MetodoBinario metBin = bucketsBinario.get(position);
                String method = "MethodBin";
                intent.putExtra("indicador",method);
                intent.putExtra("metBin", metBin);
                startActivity(intent);
            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Comprovamos que la foto se a realizado
        if (requestCode == REQUEST_CAMERA && resultCode == RESULT_OK) {
            Uri uri = file;
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap prueba = Bitmap.createScaledBitmap(bitmap,256,256,true);
            String imageFileName = getFileName(uri);
            LBP(prueba,imageFileName);
            PIXEL(prueba,imageFileName);
            System.out.println(imageFileName);
            System.out.println(bucketsBinario);
        }else{
            //La imagen escogida es copiada a una nueva en la appFolder, donde primero se pasa a bitMap y luego a imagen
            Uri uri = data.getData();
            Bitmap bitmap = null;
            try {
                bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Bitmap prueba = Bitmap.createScaledBitmap(bitmap,256,256,true);
            File folder = Environment.getExternalStoragePublicDirectory("/AppFolder");
            System.out.println(folder.getAbsolutePath());
            //Si no existe se crea
            if (!folder.exists()) {folder.mkdir();}
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = timeStamp + ".jpg";
            LBP(prueba,imageFileName);
            PIXEL(prueba,imageFileName);
            File image = new File(folder, imageFileName);
            FileOutputStream out = null;
            try {
                out = new FileOutputStream(image);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    if (out != null) {
                        out.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = getContentResolver().query(uri, null, null, null, null);
            try{
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    public void createPlanesPixel(){
        for(int i=0; i<10; i++){
            ArrayList<Integer> plane = new ArrayList<>();
            for(int j=0; j<65536; j++)
                plane.add((int)(Math.random()*513)-256);
            planesPixel.add(plane);
        }
    }

    public void createPlanes(){
        for(int i=0; i<10; i++){
            ArrayList<Integer> plane = new ArrayList<>();
            for(int j=0; j<256; j++)
                plane.add((int)(Math.random()*513)-256);
            planes.add(plane);
        }
    }

    public void PIXEL(Bitmap image, String nameImage){
        int[] pixeles = new int[image.getHeight() * image.getWidth()];
        image.getPixels(pixeles, 0, image.getWidth(), 0, 0, image.getWidth(), image.getHeight());
        String hashImage = calcularHashPixel(pixeles);
        System.out.println("nameImage : "+nameImage);
        addImagePixel(hashImage,nameImage);
    }

    public void LBP(Bitmap imagen, String nombreImagen){
        int alto = imagen.getHeight();
        int ancho = imagen.getWidth();
        int[][] matrizLBP = new int[alto][ancho];
        for (int i=1; i<ancho-1; i++){
            for (int j=1; j<alto-1; j++){
                matrizLBP[j][i] = calcularLBP(imagen,i,j);
            }
        }
        int[] histograma = new int[256];
        for (int i=0; i<256; i++)
            histograma[i] = 0;
        for (int i=1; i<ancho-1; i++){
            for (int j=1; j<alto-1; j++){
                int indice = matrizLBP[i][j];
                histograma[indice] += 1;
            }
        }
        String hash = hashImagen(histograma);
        agregarImagen(hash,nombreImagen);
    }

    public String calcularHashPixel(int[] pixImage){
        String hash = "";
        long productoPunto = 0;
        for(int i=0; i<planesPixel.size(); i++){
            for(int j=0; j<planesPixel.get(i).size(); j++)
                productoPunto+= planesPixel.get(i).get(j)*pixImage[j];
            if(productoPunto>=0)
                hash+="1";
            else
                hash+="0";
            productoPunto=0;
        }
        return hash;
    }

    public String hashImagen(int[] histograma){
        String hash = "";
        long productoPunto = 0;
        for(int i=0; i<planes.size(); i++){
            for(int j=0; j<256; j++){
                productoPunto+= planes.get(i).get(j)*histograma[j];
            }
            if(productoPunto>=0){
                hash+="1";
            }else{
                hash+="0";
            }
            productoPunto=0;
        }
        return hash;
    }

    public int calcularLBP(Bitmap imagen, int fila, int columna){
        int valorPixel = imagen.getPixel(fila,columna);
        String LBP = "";
        if (imagen.getPixel(fila-1,columna-1)>valorPixel)
            LBP += "1";
        else{
            LBP += "0";
        }
        if (imagen.getPixel(fila-1,columna)>valorPixel)
            LBP += "1";
        else {
            LBP += "0";
        }
        if (imagen.getPixel(fila-1,columna+1)>valorPixel)
            LBP += "1";
        else {
            LBP += "0";
        }
        if (imagen.getPixel(fila,columna-1)>valorPixel)
            LBP += "1";
        else {
            LBP += "0";
        }
        if (imagen.getPixel(fila,columna+1)>valorPixel)
            LBP += "1";
        else {
            LBP += "0";
        }
        if (imagen.getPixel(fila+1,columna-1)>valorPixel)
            LBP += "1";
        else {
            LBP += "0";
        }
        if (imagen.getPixel(fila+1,columna)>valorPixel)
            LBP += "1";
        else {
            LBP += "0";
        }
        if (imagen.getPixel(fila+1,columna+1)>valorPixel)
            LBP += "1";
        else {
            LBP += "0";
        }
        int valorHistograma = Integer.parseInt(LBP,2);
        return valorHistograma;
    }

    public void agregarImagen (String hash, String nombreImagen){
        for (int i=0; i<bucketsBinario.size(); i++){
            if (bucketsBinario.get(i).getHash().equals(hash)) {
                bucketsBinario.get(i).agregarImagen(nombreImagen);
                return;
            }
        }
        MetodoBinario newBin = new MetodoBinario(hash);
        newBin.agregarImagen(nombreImagen);
        bucketsBinario.add(newBin);
    }

    public void addImagePixel (String hash, String nombreImagen){
        for (int i=0; i<bucketsPixel.size(); i++){
            if (bucketsPixel.get(i).getHash().equals(hash)) {
                bucketsPixel.get(i).agregarImagen(nombreImagen);
                return;
            }
        }
        MethodPixel newPix = new MethodPixel(hash);
        newPix.agregarImagen(nombreImagen);
        bucketsPixel.add(newPix);
    }

    public static float disCoseno( int[] a, int[] b){
        float numerador = 0, denominador1 = 0, denominador2 = 0;
        for(int i = 0; i<5; i++){
            numerador = numerador + (a[i] * b[i]);
            denominador1 = denominador1 + a[i]*a[i];
            denominador2 = denominador2 + b[i]*b[i];
        }
        denominador1 = (float) Math.sqrt(denominador1);
        denominador2 = (float) Math.sqrt(denominador2);

        float result = numerador /(denominador1*denominador2);
        return result;
    }

}
