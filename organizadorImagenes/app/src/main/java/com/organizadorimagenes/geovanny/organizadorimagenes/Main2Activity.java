package com.organizadorimagenes.geovanny.organizadorimagenes;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class Main2Activity extends AppCompatActivity {
    private Button btnCambiar;
    private EditText newName;
    private ListView buckList_item;
    private ArrayAdapter<String> buckAdapter_item;
    private ArrayList<String> buckArrayList_item = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String indicador = (String) bundle.get("indicador");
        if (indicador.equals("MethodPix")){
            final MethodPixel hashPix = (MethodPixel) getIntent().getSerializableExtra("metPix");
            buckArrayList_item.clear();
            buckList_item = (ListView) findViewById(R.id.list_images);
            buckAdapter_item = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,buckArrayList_item);
            buckList_item.setAdapter(buckAdapter_item);

            for(int i=0; i<hashPix.getImages().size(); i++)
                buckArrayList_item.add(hashPix.getImages().get(i));
            buckAdapter_item.notifyDataSetChanged();
            buckList_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getApplicationContext(), MainActivityImage.class);
                    intent.putExtra("imagen", hashPix.getImages().get(position));
                    startActivity(intent);
                }
            });
        }else if (indicador.equals("MethodBin")){
            final MetodoBinario hashBin = (MetodoBinario) getIntent().getSerializableExtra("metBin");
            buckArrayList_item.clear();
            buckList_item = (ListView) findViewById(R.id.list_images);
            buckAdapter_item = new ArrayAdapter<>(getApplicationContext(),android.R.layout.simple_spinner_dropdown_item,buckArrayList_item);
            buckList_item.setAdapter(buckAdapter_item);

            for(int i=0; i<hashBin.getImagenes().size(); i++)
                buckArrayList_item.add(hashBin.getImagenes().get(i));
            buckAdapter_item.notifyDataSetChanged();
            buckList_item.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent = new Intent(getApplicationContext(), MainActivityImage.class);
                    intent.putExtra("imagen", hashBin.getImagenes().get(position));
                    startActivity(intent);
                }
            });
        }
        btnCambiar = (Button) findViewById(R.id.buttonCambiar);
        newName = (EditText) findViewById(R.id.name);
        btnCambiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = getIntent();
                Bundle bundle = intent.getExtras();
                String indicador = (String) bundle.get("indicador");
                if (indicador.equals("MethodPix")){
                    MethodPixel hashPix = (MethodPixel) getIntent().getSerializableExtra("metPix");
                    String nuevoNombre = newName.getText().toString();
                    System.out.println("NewName : "+nuevoNombre);
                    if (!nuevoNombre.equals(""))
                        hashPix.setName(newName.getText().toString());
                    System.out.println("NewName : "+hashPix.getName());
                }else if (indicador.equals("MethodBin")){
                    final MetodoBinario hashBin = (MetodoBinario) getIntent().getSerializableExtra("metBin");
                    String nuevoNombre = newName.getText().toString();
                    if (!nuevoNombre.equals(""))
                        hashBin.setName(newName.getText().toString());
                }
            }
        });
    }
}
