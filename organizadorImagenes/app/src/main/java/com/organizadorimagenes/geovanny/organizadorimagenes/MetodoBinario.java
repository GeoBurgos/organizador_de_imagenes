package com.organizadorimagenes.geovanny.organizadorimagenes;

import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Fabricio on 30/5/2017.
 */
public class MetodoBinario implements Serializable{
    public String hash;
    public ArrayList<String> imagenes;
    private String name;

    public MetodoBinario(String hash){
        this.hash = hash;
        this.name = hash;
        imagenes = new ArrayList<>();

    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public ArrayList<String> getImagenes() {
        return imagenes;
    }

    public void setImagenes(ArrayList<String> imagenes) {
        this.imagenes = imagenes;
    }

    public void agregarImagen(String imagen){
        imagenes.add(imagen);
    }

    public String buscarImagen(String imagen){
        for (int i=0; i<imagenes.size(); i++){
            if (imagenes.get(i) == imagen)
                return imagenes.get(i);
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
