package com.organizadorimagenes.geovanny.organizadorimagenes;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import java.io.File;
import java.io.FileOutputStream;

public class MainActivityImage extends AppCompatActivity {
    ImageView vistaImagen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_image);
        String nombreImagen = getIntent().getExtras().getString("imagen");
        vistaImagen = (ImageView) findViewById(R.id.imageView2);
        File imgFile =  Environment.getExternalStoragePublicDirectory("/AppFolder");
        String path = imgFile.getAbsolutePath() + "/" + nombreImagen;
        if(imgFile.exists()){

            Bitmap myBitmap = BitmapFactory.decodeFile(path);
            System.out.println("Encontrado");
            vistaImagen.setImageBitmap(myBitmap);

        }
    }
}
